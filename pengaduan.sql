# Host: localhost  (Version 5.6.21)
# Date: 2017-04-20 03:17:26
# Generator: MySQL-Front 5.3  (Build 5.39)

/*!40101 SET NAMES latin1 */;

#
# Structure for table "kabupaten"
#

DROP TABLE IF EXISTS `kabupaten`;
CREATE TABLE `kabupaten` (
  `kode` varchar(4) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kode_prop` varchar(2) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Data for table "kabupaten"
#

/*!40000 ALTER TABLE `kabupaten` DISABLE KEYS */;
INSERT INTO `kabupaten` VALUES ('1101','SIMEULUE','11'),('1102','ACEH SINGKIL','11'),('1103','ACEH SELATAN','11'),('1104','ACEH TENGGARA','11'),('1105','ACEH TIMUR','11'),('1106','ACEH TENGAH','11'),('1107','ACEH BARAT','11'),('1108','ACEH BESAR','11'),('1109','PIDIE','11'),('1110','BIREUEN','11'),('1111','ACEH UTARA','11'),('1112','ACEH BARAT DAYA','11'),('1113','GAYO LUES','11'),('1114','ACEH TAMIANG','11'),('1115','NAGAN RAYA','11'),('1116','ACEH JAYA','11'),('1171','KOTA BANDA ACEH','11'),('1172','KOTA SABANG','11'),('1173','KOTA LANGSA','11'),('1174','KOTA LHOKSUMAWE','11'),('1201','Nias','12'),('1202','Mandailing Natal','12'),('1203','Tapanuli Selatan','12'),('1204','Tapanuli Tengah','12'),('1205','Tapanuli Utara','12'),('1207','Labuhan Batu','12'),('1208','Asahan','12'),('1209','Simalungun','12'),('1210','Dairi','12'),('1211','Karo','12'),('1213','Langkat','12'),('1214','Nias Selatan','12'),('1215','Humbang Hasundutan','12'),('1216','Pak-Pak Bharat','12'),('1217','Toba Samosir','12'),('1218','Deli Serdang','12'),('1271','Kota Sibolga','12'),('1272','Kota Tanjung Balai','12'),('1273','Kota Pematang Siantar','12'),('1274','Kota Tebing Tinggi','12'),('1275','Kota Medan','12'),('1276','Kota Binjai','12'),('1277','Kota Padang Sidempuan','12'),('1301','Kepulauan Mentawai','13'),('1302','Pasisir Selatan','13'),('1304','Sawahlunto/Sijunjung','13'),('1306','Padang Pariaman','13'),('1307','Tanah Datar','13'),('1308','Solok','13'),('1309','Pasaman','13'),('1310','Agam','13'),('1311','Dharmas Raya','13'),('1312','Pasaman Barat','13'),('1313','Lima Puluh Koto','13'),('1371','Kota Padang','13'),('1372','Kota Solok','13'),('1373','Kota Sawah Lunto','13'),('1374','Kota Padang Panjang','13'),('1375','Kota Bukittinggi','13'),('1376','Kota Payakumbuh','13'),('1377','Kota Pariaman','13'),('1401','Kuantan Singingi','14'),('1402','Indragiri Hulu','14'),('1403','Indragiri Hilir','14'),('1404','Pelalawan','14'),('1405','Siak','14'),('1406','Kampar','14'),('1407','Rokan Hulu','14'),('1408','Bengkalis','14'),('1409','Rokan Hilir','14'),('1471','Kota Pekan Baru','14'),('1473','Kota Dumai','14'),('1501','Kerinci','15'),('1502','Merangin','15'),('1503','Sarolangun','15'),('1504','Batang Hari','15'),('1505','Muaro Jambi','15'),('1506','Tanjung Jabung Timur','15'),('1507','Tanjung Jabung Barat','15'),('1508','Tebo','15'),('1509','Bungo','15'),('1571','Kota Jambi','15'),('1603','Muara Enim','16'),('1604','Lahat','16'),('1605','Musi Rawas','16'),('1606','Musi Banyu Asin','16'),('1607','Banyu Asin','16'),('1608','Ogan Komering Ulu','16'),('1610','Ogan Komering Ilir','16'),('1671','Kota Palembang','16'),('1672','Kota Prabumulih','16'),('1673','Kota Pagar Alam','16'),('1674','Kota Lubuklinggau','16'),('1701','Bengkulu Selatan','17'),('1703','Bengkulu Utara','17'),('1704','Seluma','17'),('1705','Kaur','17'),('1706','Muko-Muko','17'),('1708','Rejang Lebong','17'),('1771','Kota Bengkulu','17'),('1801','Lampung Barat','18'),('1802','Tanggamus','18'),('1803','Lampung Selatan','18'),('1804','Lampung Timur','18'),('1805','Lampung Tengah','18'),('1806','Lampung Utara','18'),('1807','Way Kanan','18'),('1808','Tulangbawang','18'),('1871','Kota Bandar Lampung','18'),('1872','Kota Metro','18'),('1901','Bangka','19'),('1902','Belitung','19'),('1903','Bangka Barat','19'),('1904','Bangka Tengah','19'),('1905','Bangka Selatan','19'),('1906','Belitung Timur','19'),('1971','Kota Pangkal Pinang','19'),('2001','Karimun','20'),('2002','Kepulauan Riau','20'),('2003','Natuna','20'),('2071','Kota Batam','20'),('2072','Kota Tanjung Pinang','20'),('3101','KAPULAUAN SERIBU','31'),('3171','KOTA JAKARTA SELATAN','31'),('3172','KOTA JAKARTA TIMUR','31'),('3173','KOTA JAKARTA PUSAT','31'),('3174','KOTA JAKARTA BARAT','31'),('3175','KOTA JAKARTA UTARA','31'),('3202','Sukabumi','32'),('3203','Cianjur','32'),('3204','Bandung','32'),('3205','Garut','32'),('3206','Tasikmalaya','32'),('3207','Ciamis','32'),('3208','Kuningan','32'),('3209','Cirebon','32'),('3210','Majalengka','32'),('3211','Sumedang','32'),('3212','Indramayu','32'),('3213','Subang','32'),('3214','Purwakarta','32'),('3215','Karawang','32'),('3271','Kdy. Bogor','32'),('3272','Kota Sukabumi','32'),('3273','Kdy. Bandung','32'),('3274','Kdy. Cirebon','32'),('3275','Bekasi','32'),('3276','Bogor','32'),('3279','Kota Banjar','32'),('3280','Kdy. Sukabumi','32'),('3301','Cilacap','33'),('3302','Banyumas','33'),('3303','Purbalingga','33'),('3304','Banjarnegara','33'),('3305','Kebumen','33'),('3306','Purworejo','33'),('3307','Wonosobo','33'),('3308','Magelang','33'),('3309','Boyolali','33'),('3310','Klaten','33'),('3311','Sukoharjo','33'),('3312','Wonogiri','33'),('3313','Karanganyar','33'),('3314','Sragen','33'),('3315','Grobogan','33'),('3316','Blora','33'),('3317','Rembang','33'),('3318','Pati','33'),('3319','Kudus','33'),('3320','Jepara','33'),('3321','Demak','33'),('3322','Semarang','33'),('3323','Temanggung','33'),('3324','Kendal','33'),('3325','Batang','33'),('3326','Pekalongan','33'),('3327','Pemalang','33'),('3328','Tegal','33'),('3329','Brebes','33'),('3371','Kdy. Magelang','33'),('3372','Kdy. Surakart','33'),('3373','Kdy. Salatiga','33'),('3374','KOTA SEMARANG','33'),('3375','Kdy. Pekalong','33'),('3376','Kdy. Tegal','33'),('3401','Kulon Progo','34'),('3402','Bantul','34'),('3403','Gunung Kidul','34'),('3404','Sleman','34'),('3471','Kota Yogyaka*','34'),('3501','Pacitan','35'),('3502','Ponorogo','35'),('3503','Trenggalek','35'),('3504','Tulungagung','35'),('3505','Blitar','35'),('3506','Kediri','35'),('3507','Malang','35'),('3508','Lumajang','35'),('3509','Jember','35'),('3510','Banyuwangi','35'),('3511','Bondowoso','35'),('3512','Situbondo','35'),('3513','Probolinggo','35'),('3514','Pasuruan','35'),('3515','Sidoarjo','35'),('3516','Mojokerto','35'),('3517','Jombang','35'),('3518','Nganjuk','35'),('3519','Madiun','35'),('3520','Magetan','35'),('3521','Ngawi','35'),('3522','Bojonegoro','35'),('3523','Tuban','35'),('3524','Lamongan','35'),('3525','Gresik','35'),('3526','Bangkalan','35'),('3527','Sampang','35'),('3528','Pamekasan','35'),('3529','Sumenep','35'),('3571','Kota Kediri','35'),('3572','Kota Blitar','35'),('3574','Kota Probolin','35'),('3575','Kota Pasuruan','35'),('3576','Kota Mojokert','35'),('3577','Kota Madiun','35'),('3578','Kota Surabaya','35'),('3579','Kota Batu','35'),('3580','Kdy. Kediri','35'),('3601','Pandeglang','36'),('3602','Lebak','36'),('3603','Tangerang','36'),('3604','Serang','36'),('3671','Kota Tangerang','36'),('3672','Kota Cilegon','36'),('5101','Jembrana','51'),('5102','Tabanan','51'),('5103','Badung','51'),('5104','Gianyar','51'),('5105','Klungkung','51'),('5106','Bangli','51'),('5107','Karangasem','51'),('5108','Buleleng','51'),('5171','Kota Denpasar','51'),('5201','Lombok Barat','52'),('5202','Lombok Tengah','52'),('5203','Lombok Timur','52'),('5204','Sumbawa','52'),('5205','Dompu','52'),('5206','Bima','52'),('5271','Kota Mataram','52'),('5272','Kota Bima','52'),('5301','Sumba Barat','53'),('5302','Sumba Timur','53'),('5303','Kupang','53'),('5304','Timor Tengah Selatan','53'),('5305','Timor Tengah Utara','53'),('5306','Belu','53'),('5307','Alor','53'),('5308','Lembata','53'),('5309','Flores Timur','53'),('5310','Sikka','53'),('5311','Ende','53'),('5312','Ngada','53'),('5313','Manggarai','53'),('5314','Rote Ndao','53'),('5315','Manggarai Barat','53'),('5371','Kota Kupang','53'),('6101','Sambas','61'),('6102','Bengkayang','61'),('6103','Landak','61'),('6104','Pontianak','61'),('6105','Sanggau','61'),('6106','Ketapang','61'),('6107','Sintang','61'),('6108','Kapuas Hulu','61'),('6109','Sekadau','61'),('6110','Melawi','61'),('6171','Kota Pontianak','61'),('6172','Kota Singkawang','61'),('6201','Kotawaringin Barat','62'),('6202','Kotawaringin Timur','62'),('6203','Kapuas','62'),('6204','Barito Selatan','62'),('6205','Barito Utara','62'),('6206','Sukamara','62'),('6207','Lamandau','62'),('6208','Seruyan','62'),('6209','Katingan','62'),('6210','Pulang Pisau','62'),('6211','Gunung Mas','62'),('6212','Barito Timur','62'),('6213','Murung Raya','62'),('6271','Palangka Raya','62'),('6301','Tanah Laut','63'),('6302','Kota Baru','63'),('6303','Banjar','63'),('6304','Barito Kuala','63'),('6305','Tapin','63'),('6306','Hulu Sungai Selatan','63'),('6307','Hulu Sungai Tengah','63'),('6308','Hulu Sungai Utara','63'),('6309','Tabalong','63'),('6310','Tanah Bumbu','63'),('6311','Balangan','63'),('6371','Kota Banjarmasin','63'),('6372','Kota Banjar Baru','63'),('6401','Pasir','64'),('6402','Kutai Barat','64'),('6403','Kutai','64'),('6404','Kutai Timur','64'),('6405','Berau','64'),('6406','Malinau','64'),('6407','Bulongan','64'),('6408','Nunukan','64'),('6409','Penajam Paser Utara','64'),('6471','Balikpapan','64'),('6472','Samarinda','64'),('6473','Tarakan','64'),('6474','Bontang','64'),('7101','Bolaang Mongondow','71'),('7102','Minahasa','71'),('7103','Sangihe Talaud','71'),('7104','Kepulauan Talaud','71'),('7105','Minahasa Selatan','71'),('7171','Manado','71'),('7172','Bitung','71'),('7173','Tomohon','71'),('7201','Banggai Kepulauan','72'),('7202','Banggai','72'),('7203','Morowali','72'),('7205','Donggala','72'),('7206','Toli-Toli','72'),('7207','Buol','72'),('7208','Parigi Moutong','72'),('7209','Poso','72'),('7271','Kota Palu','72'),('7301','Selayar','73'),('7302','Bulukumba','73'),('7303','Bantaeng','73'),('7304','Jeneponto','73'),('7305','Takalar','73'),('7306','Gowa','73'),('7307','Sinjai','73'),('7308','Maros','73'),('7309','Pangkajene Kepulauan','73'),('7310','Barru','73'),('7311','Bone','73'),('7312','Soppeng','73'),('7313','Wajo','73'),('7314','Sidenreng Rappang','73'),('7315','Pinrang','73'),('7316','Enrekang','73'),('7317','Luwu','73'),('7318','Tana Toraja','73'),('7325','Luwu Timur','73'),('7371','Luwu Utara','73'),('7372','Kota Pare-Pare','73'),('7373','Kota Palopo','73'),('7380','Kota Ujung Pandang','73'),('7401','Buton','74'),('7402','Muna','74'),('7403','Kendari','74'),('7404','Kolaka','74'),('7405','Konawe Selatan','74'),('7471','Kota Kendari','74'),('7472','Kota Bau-Bau','74'),('7501','Boalemo','75'),('7502','Gorontalo','75'),('7503','Pohuwato','75'),('7504','Bone Bolango','75'),('7509','Gorontalo Utara','75'),('7571','Kota Gorontalo','75'),('7619','Polewali Mamasa','76'),('7620','Majene','76'),('7621','Mamuju','76'),('7623','Mamasa','76'),('7624','Mamuju Utara','76'),('8101','Maluku Tenggara Bara','81'),('8102','Maluku Tenggara','81'),('8104','Buru','81'),('8107','Maluku Tengah','81'),('8171','Kota Ambon','81'),('8201','Maluku Utara','82'),('8202','Halmahera Tengah','82'),('8203','Kepulauan Sula','82'),('8204','Halmahera Selatan','82'),('8205','Halmahera Utara','82'),('8206','Halmahera Timur','82'),('8271','Kota Ternate','82'),('8272','Kota Tidore','82'),('9401','Merauke','94'),('9402','Jayawijaya','94'),('9403','Jayapura','94'),('9404','Nabire','94'),('9408','Yapen Waropen','94'),('9409','Biak Numfor','94'),('9410','Paniai','94'),('9411','Puncak Jaya','94'),('9412','Mimika','94'),('9413','Boven Digoel','94'),('9414','Mappi','94'),('9415','Asmat','94'),('9416','Yahukimo','94'),('9417','Pegunungan Bintang','94'),('9418','Tolikara','94'),('9419','Sarmi','94'),('9420','Keerom','94'),('9426','Waropen','94'),('9471','Kota Jayapura','94'),('9501','Fak-Fak','95'),('9502','Kaimana','95'),('9503','Teluk Wondama','95'),('9504','Teluk Bintuni','95'),('9505','Manokwari','95'),('9506','Sorong Selatan','95'),('9507','Sorong','95'),('9508','Raja Ampat','95'),('9571','Kota Sorong','95');
/*!40000 ALTER TABLE `kabupaten` ENABLE KEYS */;

#
# Structure for table "pengaduan"
#

DROP TABLE IF EXISTS `pengaduan`;
CREATE TABLE `pengaduan` (
  `Id_pengaduan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pengadu` varchar(255) DEFAULT NULL,
  `alamat_pengadu` varchar(255) DEFAULT NULL,
  `no_surat` varchar(255) DEFAULT NULL,
  `keterangan_surat` varchar(100) DEFAULT NULL,
  `tgl_surat` varchar(20) DEFAULT NULL,
  `nama_pemohon` varchar(50) DEFAULT NULL,
  `jenis_kelamin` char(1) DEFAULT NULL,
  `tempat_lahir_pemohon` varchar(255) DEFAULT NULL,
  `tanggal_lahir_pemohon` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `kabupaten` varchar(255) DEFAULT NULL,
  `provinsi` varchar(255) DEFAULT NULL,
  `no_kontak` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `hubungan` varchar(255) DEFAULT NULL,
  `no_ktp_pemohon` varchar(255) DEFAULT NULL,
  `no_kk_pemohon` varchar(255) DEFAULT NULL,
  `no_surat_lainnya` varchar(255) DEFAULT NULL,
  `no_bap` varchar(255) DEFAULT NULL,
  `kronologis` varchar(255) DEFAULT NULL,
  `ket_organisasi` varchar(255) DEFAULT NULL,
  `dok_lain` varchar(255) DEFAULT NULL,
  `peristiwa_pel_ham_berat` varchar(255) DEFAULT NULL,
  `pembunuhan` varchar(255) DEFAULT NULL,
  `pemusnahan` varchar(255) DEFAULT NULL,
  `perbudakan` varchar(255) DEFAULT NULL,
  `pengusiran` varchar(255) DEFAULT NULL,
  `perampasan` varchar(255) DEFAULT NULL,
  `penyiksaaan` varchar(255) DEFAULT NULL,
  `perkosaan` varchar(255) DEFAULT NULL,
  `penganiayaan` varchar(255) DEFAULT NULL,
  `penghilangan_paksa` varchar(255) DEFAULT NULL,
  `kejahatan_apartheid` varchar(255) DEFAULT NULL,
  `perbuatan_lainnya` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id_pengaduan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "pengaduan"
#


#
# Structure for table "propinsi"
#

DROP TABLE IF EXISTS `propinsi`;
CREATE TABLE `propinsi` (
  `kode` varchar(2) NOT NULL,
  `nama` varchar(40) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Data for table "propinsi"
#

/*!40000 ALTER TABLE `propinsi` DISABLE KEYS */;
INSERT INTO `propinsi` VALUES ('11','NANGGROE ACEH DARUSSALAM'),('12','SUMATERA UTARA'),('13','SUMATERA BARAT'),('14','RIAU'),('15','JAMBI'),('16','SUMATERA SELATAN'),('17','BENGKULU'),('18','LAMPUNG'),('19','BANGKA BELITUNG'),('20','KEPULAUAN RIAU'),('31','DKI JAKARTA'),('32','JAWA BARAT'),('33','JAWA TENGAH'),('34','DI YOGYAKARTA'),('35','JAWA TIMUR'),('36','BANTEN'),('51','BALI'),('52','NUSA TENGGARA BARAT'),('53','NUSA TENGGARA TIMUR'),('61','KALIMANTAN BARAT'),('62','KALIMANTAN TENGAH'),('63','KALIMANTAN SELATAN'),('64','KALIMANTAN TIMUR'),('71','SULAWESI UTARA'),('72','SULAWESI TENGAH'),('73','SULAWESI SELATAN'),('74','SULAWESI TENGGARA'),('75','GORONTALO'),('76','SULAWESI BARAT'),('81','MALUKU'),('82','MALUKU UTARA'),('94','PAPUA'),('95','IRIAN JAYA BARAT');
/*!40000 ALTER TABLE `propinsi` ENABLE KEYS */;

#
# Structure for table "user"
#

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `no_ktp` char(16) NOT NULL DEFAULT '' COMMENT 'No KTP',
  `nama` varchar(50) DEFAULT NULL COMMENT 'Nama',
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `alamat` varchar(150) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`no_ktp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "user"
#

INSERT INTO `user` VALUES ('1234567891234567','Anggit Prayogo','oKjsxM3V','anggitprayogo@gmail.com','$2y$10$y9iUWAvWwhOuPtbKGeztw.IE2Xud2mGhpd/pGYlBPJ1C81gKnLn3m','+6287812035533','Jl Nyimas Melati,Kp. Pondok Kelor, RT 001/001, Ds. Pondok Kelor, Sepatan Timur',NULL,0),('3603300111660003','Ali Suherman','aliherman','ali@gmail.com','$2y$10$L0YxI4KDiT8Xysq4d1kM0.HhiftLLFG0jBpqFRK8.PoK1SFDGYEeW','08188829109','Jalan Nyimas Melati Rt 001/001 Sepatan Timur Tangerang',NULL,0);
