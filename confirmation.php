<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Home .:. Pengaduan Online .:.</title>
  <link rel="stylesheet" href="css/bootstrap.css">
  <script src="js/jquery-2.2.2.js"></script>
  <script src="js/bootstrap.js"></script>
  <style>
		.jumbotron{
			background-color: #F0F3F6;
			width: 70%;
		}
		.container .jumbotron, .container-fluid .jumbotron {
		    border-radius: 1px;
		    border-left: 5px solid #01838F;
		    border-bottom:2px solid #01838F;
		}
		.btn{
			border-radius: 1px;
		}
	</style>
</head>
<body>
  <?php include 'include/navbar.php'; ?>

	<div class="container">
		<div class="row">
			<div class="col-lg-3"></div>
			<div class="col-lg-9 col-sm-12">
				<div class="jumbotron">
				  <h3>Konfirmasi</h3>
				  <hr>
				  <p>Silahkan konfirmasi pendaftaran melalui email</p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>