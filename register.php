<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Pendaftaran .:. Pengaduan Online .:.</title>
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/style.css">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="js/jquery-2.2.2.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="js/jquery.validate.js"></script>
  <script src="js/carousel.js"></script>
</head>
<body>
 	<?php include 'include/navbar.php'; ?>
 	<?php include 'include/register_form.php' ?>
 	<?php include 'include/footer.php' ?>
</body>
</html>