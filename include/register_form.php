<style>
	.btn{
		border-radius: 1px;
	}
	label{
		color: white;
	}
</style>

	<div class="container">

	<div class="row">
	    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<form role="form" class="form_registration" id="commentForm" method="post" action="regis_proses.php">
				<h2 style="color: white;">Silahkan Mendaftar</h2>
				<hr class="colorgraph">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">
	                        <input type="text" id="cname" name="first_name" class="form-control input-lg" placeholder="Nama Depan" tabindex="1" required="">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">
							<input type="text" name="last_name" id="last_name" class="form-control input-lg" placeholder="Nama Belakang" tabindex="2" required="">
						</div>
					</div>
				</div>
				<div class="form-group">
					<input type="text" name="no_ktp" id="no_ktp" class="form-control input-lg" placeholder="No KTP" tabindex="3" required minlength="16" maxlength="16">
				</div>
				<div class="form-group">
					<input type="email" name="email" id="cmail" class="form-control input-lg" placeholder="Email" tabindex="3" required="">
				</div>
				<div class="form-group">
					<input type="tel" name="tel" id="tel" class="form-control input-lg" placeholder="No. HP/Telp" tabindex="4" required="">
				</div>
				<div class="form-group">
					<textarea name="alamat" id="alamat" cols="30" rows="10" class="form-control input-lg" placeholder="Alamat" required=""></textarea>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12"  style="color: white;">
						 By clicking <strong class="label label-primary">Daftar</strong>, you agree to the <a href="#" data-toggle="modal" data-target="#t_and_c_m">Terms and Conditions</a> set out by this site, including our Cookie Use.
					</div>
				</div>
				
				<hr class="colorgraph">
				<div class="row">
					<div class="col-xs-12 col-md-12"><input type="submit" value="Daftar" class="btn btn-primary btn-block btn-lg" tabindex="7" name="daftar"></div>
					<div class="col-xs-12 col-md-12"><center><h4 style="color: white;">Sudah terdaftar? Login <a href="login.php">disini</a></h4></center></div>
				</div>
			</form>
		</div>
	</div>
	</div>

<script>
	$("#commentForm").validate();
</script>

