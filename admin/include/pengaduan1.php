<?php  

if (isset($_POST['submit1'])) {
	foreach ($_POST as $key => $value) {
		${$key} = $value;
	}

	
	$_SESSION['nama_pengadu'] = $nama_pengadu;
	$_SESSION['alamat_pengadu'] = $alamat_pengadu;
	$_SESSION['no_surat_komnas'] = $no_surat_komnas;
	$_SESSION['ket_surat'] = $ket_surat;
	$_SESSION['tgl_surat'] = $tgl_surat;

}

#ambil data propinsi
$query = "SELECT kode, nama FROM propinsi ORDER BY nama";
$sql = mysqli_query($conn, $query);
$arrpropinsi = array();
while ($row = mysqli_fetch_assoc($sql)) {
	$arrpropinsi [ $row['kode'] ] = $row['nama'];
}

#action get Kabupaten
if(isset($_GET['action']) && $_GET['action'] == "getKab") {
	$kode_prop = $_GET['kode_prop'];
	
	//ambil data kabupaten
	$query = "SELECT kode, nama FROM kabupaten WHERE kode_prop='$kode_prop' ORDER BY nama";
	$sql = mysqli_query($conn, $query);
	$arrkab = array();
	while ($row = mysqli_fetch_assoc($sql)) {
		array_push($arrkab, $row);
	}
	echo json_encode($arrkab);
	exit;
}
?>

<style>
	.btn{
		border-radius: 1px;
	}
	label{
		color: white;
		font-size: 18px;
	}
	legend{
		color: white;
	}
	fieldset{
		margin-top: 20px;
	}
</style>

<script type="text/javascript">
	$(document).ready(function(){
		$('#propinsi').change(function(){
			$.getJSON('input_aduan.php',{action:'getKab', kode_prop:$(this).val()}, function(json){
				$('#kabupaten').html('');
				$.each(json, function(index, row) {
					$('#kabupaten').append('<option value='+row.kode+'>'+row.nama+'</option>');
				});
			});
		});
	});
</script>

	<div class="container">

	<div class="row">
	    <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">
			<form role="form" class="form_registration" id="commentForm" method="post" action="">
				<h2 style="color: white;">Form Pengaduan</h2>
				<hr class="colorgraph">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="form-group">
							<label for="">Nama Pemohon : </label>
	                        <input type="text" id="cname" name="name" class="form-control input-lg" placeholder="Nama Pemohon" tabindex="1" required="">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="radio">Jenis Kelamin : </label>
					<label class="radio-inline" style="color: white;font-size: 18px;"><input type="radio" name="optradio">Laki-laki</label>
					<label class="radio-inline" style="color: white;font-size: 18px;"><input type="radio" name="optradio">Perempuan</label>
				</div>
				<div class="form-group">
					<label for="">Tempat Lahir : </label>
					<input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control input-lg" placeholder="Tempat Lahir" tabindex="3" required="">
				</div>
				<div class="form-group">
					<label for="">Tanggal Lahir : </label>
					<input type="text" name="tgl_lahir" id="datepicker" class="form-control input-lg" placeholder="Tanggal Lahir" tabindex="3" required="">
				</div>
				<div class="form-group">
					<label for="">Provinsi : </label>
					<select id="propinsi" name="propinsi" class="form-control">
						<option value="">-Pilih-</option>
						<?php
						foreach ($arrpropinsi as $kode=>$nama) {
							echo "<option value='$kode'>$nama</option>";
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<label for="">Kota/Kabupaten : </label>
					<select id="kabupaten" name="kabupaten" class="form-control">
					</select>
				</div>
				<div class="form-group">
					<label for="">Alamat : </label>
					<textarea name="alamat" id="alamat" cols="30" rows="10" class="form-control input-lg" placeholder="Alamat" required=""></textarea>
				</div>
				<div class="form-group">
					<label for="">Nomor Kontak : </label>
					<input type="tel" name="no_kontak" id="no_kontak" class="form-control input-lg" placeholder="Nomor Kontak" tabindex="3" required="">
				</div>
				
				<fieldset>
					<legend>Status Terkait Pristiwa</legend>
					<div class="form-group">
						<label for="">Korban/Keluarga korban : </label>
						<select id="status" name="status" class="form-control input-lg">
							<option value="korban">Korban</option>
							<option value="keluarga_korban">Keluarga Korban</option>
						</select>
					</div>
					<div class="form-group">
						<label for="">Hubungan : </label>
						<input type="text" name="hubungan" class="form-control input-lg" placeholder="Hubungan">
					</div>
				</fieldset>
				
				
				<fieldset>
					<legend>Dokumen Identitas</legend>
					<div class="form-group">
						<label for="">No.KTP : </label>
						<input type="text" class="form-control input-lg" placeholder="No.KTP" name="no_ktp">
					</div>
					<div class="form-group">
						<label for="">No.Kartu Keluarga : </label>
						<input type="text" name="no_kk" class="form-control input-lg" placeholder="No.KK">
					</div>
					<div class="form-group">
						<label for="">No.Surat Lainnya : </label>
						<input type="text" name="no_lainnya" class="form-control input-lg" placeholder="No Surat Lainnya">
					</div>
				</fieldset>
				
				<fieldset>
					<legend>Perbuatan yang dialami oleh korban berdasarkan UU 26</legend>
					<div class="checkbox">
					  <label><input type="checkbox" value="pembunuhan" name="pembunuhan">Pembunuhan</label>
					</div>
					<div class="checkbox">
					  <label><input type="checkbox" value="pemusnahan" name="pemusnahan">Pemusnahan</label>
					</div>
					<div class="checkbox">
					  <label><input type="checkbox" value="perbudakan" name="perbudakan">Perbudakan</label>
					</div>
					<div class="checkbox">
					  <label><input type="checkbox" value="pengusiran" name="pengusiran">Pengusiran</label>
					</div>
					<div class="checkbox">
					  <label><input type="checkbox" value="perampasan kemerdekaan" name="perampasan_kemerdekaan">Perampasan Kemerdekaan</label>
					</div>
					<div class="checkbox">
					  <label><input type="checkbox" value="penyiksaan" name="penyiksaan">Penyiksaan</label>
					</div>
					<div class="checkbox">
					  <label><input type="checkbox" value="perkosaan" name="perkosaan">Perkosaan</label>
					</div>
					<div class="checkbox">
					  <label><input type="checkbox" value="penganiayaan" name="penganiayaan">Penganiayaan</label>
					</div>
					<div class="checkbox">
					  <label><input type="checkbox" value="penghilangan paksa" name="penghilangan_paksa">Penghilangan Paksa</label>
					</div>
					<div class="checkbox">
					  <label><input type="checkbox" value="kejahatan apartheid" name="kejahatan_apartheid">Kejahatan Apartheid</label>
					</div>
					<div class="checkbox">
					  <label><input type="checkbox" value="lainnya" name="lainnya">Kejahatan Lainnya</label>
					</div>
				</fieldset>				
					
				<hr class="colorgraph">
				<div class="row">
					<div class="col-xs-12 col-md-6"><input type="reset" value="Reset" class="btn btn-danger btn-block btn-lg" tabindex="7"></div>
					<div class="col-xs-12 col-md-6"><input type="submit" value="Next" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
				</div>
			</form>
		</div>
	</div>
	</div>

<script>
	$("#commentForm").validate();

	$( function() {
	  $( "#datepicker" ).datepicker();
	} );
</script>