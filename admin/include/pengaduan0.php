<style>
	.btn{
		border-radius: 1px;
	}
	label{
		color: white;
		font-size: 18px;
	}
</style>

<div class="container">

	<div class="row">
	    <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">
			<form role="form" class="form_registration" id="commentForm" method="post" action="input_aduan2.php">
				<h2 style="color: white;">Form Pengaduan</h2>
				<hr class="colorgraph">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="form-group">
							<label for="">Nama Pengadu : </label>
	                        <input type="text" id="cname" name="nama_pengadu" class="form-control input-lg" placeholder="Nama Pengadu" tabindex="1" required="">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="">Alamat Pengadu: </label>
					<textarea name="alamat_pengadu" id="alamat_pengadu" cols="30" rows="10" class="form-control input-lg" placeholder="Alamat Pengadu" required=""></textarea>
				</div>
				<div class="form-group">
					<label for="">Nomor Surat Komnas HAM : </label>
					<input type="text" name="no_surat_komnas" id="no_surat_komnas" class="form-control input-lg" placeholder="Nomor Surat Komnas HAM" tabindex="3" required="">
				</div>
				<div class="form-group">
					<label for="">Keterangan Surat : </label>
					<input type="text" name="ket_surat" id="ket_surat" class="form-control input-lg" placeholder="Keterangan Surat" tabindex="3" required="">
				</div>
				<div class="form-group">
					<label for="">Tanggal Surat Komnas HAM : </label>
					<input type="text" name="tgl_surat" id="datepicker" class="form-control input-lg" placeholder="Tanggal Surat" tabindex="3" required="">
				</div>
				<hr class="colorgraph">
				<div class="row">
					<div class="col-xs-12 col-md-6"><input type="reset" value="Reset" class="btn btn-danger btn-block btn-lg" tabindex="7"></div>
					<div class="col-xs-12 col-md-6"><input type="submit" value="Next" name="submit1" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
				</div>
			</form>
		</div>
	</div>
	</div>
